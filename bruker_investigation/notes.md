## Investigating Bruker problems

When performing a scan or a count the bruker will radomly scale all values by 10e8 or 10e9. 

I test with "check done" set to .1 second and read spectrum to passive. 

### Testing with Valve Closed

I close the valve in front of the Bruker then I power cycle the devices.

#### Test 1 600 Kcps no Light

1 second integration time. 

![alt text](600kcps_no_light.png)

![alt text](600kcps_no_light_spikes.png)

#### Test 2 400 Kcps no Light

1 second integration time. 

![alt text](400Kcps_no_light.png)

![alt text](400Kcps_no_light_no_spikes.png)

#### Test 3 275 Kcps no Light

1 second integration time. 

![alt text](275Kcps_no_light.png)

![alt text](<Screenshot from 2024-10-24 13-08-57.png>)

### Testing with the valve open with light


#### Test 4 600 Kcps With hard light 4.97 e9 on the M4 mirror 4900 eV

![alt text](image.png)

![alt text](image-1.png)

#### Test 5 400 Kcps With hard light 4.97 e9 on the M4 mirror 4900 eV

The device saturates

![alt text](image-3.png)

![alt text](image-4.png)


This image is taken after closing the v11 and shutting off the light into the chamber

![alt text](image-5.png)

Strange artefact still there, showing that the detector remains saturated... 

![alt text](image-6.png)


I did one last scan with the valve closed, after it was in this saturated mode. 
Now we see that what worked before (400Kcps) does not work after it was saturated

![alt text](image-7.png)

Now we have to power cycle the Bruker because it has to recover. 

### Test Chamber Light on and off. 

We observed that the atefact above is in fact caused by the chamber light... Not sure if it's electrical interferance or?

#### Here is 
![alt text](with_chamber_light_off_no_beam.png)

