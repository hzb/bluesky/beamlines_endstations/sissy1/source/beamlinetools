from bluesky.plans import count as _count, scan as _scan, grid_scan as _grid_scan
from bluesky.plan_stubs import mov as _mov, movr as _movr, abs_set as _abs_set, configure as _configure, wait as _wait, sleep as _sleep
import ophyd
from bessyii.plans.flying import flyscan as _flyscan
from bessyii.plans.flying import mov_count
from bluesky_queueserver import parameter_annotation_decorator
import numpy as np
from bluesky import Msg

print('\n\nLOADING plans.py')

from beamlinetools.beamline_config.beamline import *

soft_detectors = [i0_mirror_current_ue48, kth20]
hard_detectors = [i0_mirror_current_u17, kth26]
all_detectors = [i0_mirror_current_u17,i0_mirror_current_ue48, kth26,pfy_bruker.mca]

sissy1_manipulator_list = ["manipulator.x", "manipulator.y", "manipulator.z", "manipulator.r1", "manipulator.r2"]

@parameter_annotation_decorator({
    "parameters": {
        "motor_1": {
            "annotation": "motor_1",
            "enums": {"motor_1": sissy1_manipulator_list},
            "convert_device_names": True,
        },

        "motor_1_start": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "motor_1_stop": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "motor_1_num": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },
        "motor_2": {
            "annotation": "motor_2",
            "enums": {"motor_2": sissy1_manipulator_list},
            "convert_device_names": True,
        },
        "motor_2_start": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "motor_2_stop": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "motor_2_num": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },

    }
})
def sample_map_custom(pfy_mca:bool=True,motor_1:str="manipulator.x",motor_1_start:float=0.0, motor_1_stop:float=0.0, motor_1_num:int=10,motor_2:str="manipulator.y", motor_2_start:float=0.0, motor_2_stop:float=0.0, motor_2_num:int=10,snake:bool=False, *, md:dict=None):

    detectors_list = hard_detectors + soft_detectors
    if pfy_mca:
        detectors_list.append(pfy_bruker.mca)

#    if XPS_analyzer:
#        detectors_list.append(XPS_analyzer)

    #configure keithleys for reliable readings
    for detector in detectors_list:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(2)

    yield from _grid_scan(detectors_list, motor_1, motor_1_start, motor_1_stop,motor_1_num, motor_2, motor_2_start, motor_2_start, motor_2_num, snake_axes=snake,md=md)  

