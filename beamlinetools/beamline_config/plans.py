from bluesky.plans import count as _count, scan as _scan, grid_scan as _grid_scan
from bluesky.plan_stubs import mov as _mov, movr as _movr, abs_set as _abs_set, configure as _configure, wait as _wait, sleep as _sleep
import ophyd
from bessyii.plans.flying import flyscan as _flyscan
from bessyii.plans.flying import mov_count
from bluesky_queueserver import parameter_annotation_decorator
import numpy as np
from bluesky import Msg

print('\n\nLOADING plans.py')

from .beamline import *
from beamlinetools.plans.plan_test import sample_map_custom

soft_detectors = [i0_mirror_current_ue48, kth20]
hard_detectors = [i0_mirror_current_u17, kth26]
all_detectors = [i0_mirror_current_u17,i0_mirror_current_ue48, kth26,pfy_bruker.mca]

# default the bruker to plot the first 3 rois
pfy_bruker_mca.roi1.display()
pfy_bruker_mca.roi2.display()
pfy_bruker_mca.roi3.display()

def _check_limits(obj, value):
    """
    Similar to the device level check limits, but always does it!
    
    """
    if value is None:
            raise ValueError("Cannot write None to epics PVs")

    low_limit, high_limit = obj.limits

    if low_limit >= high_limit:
        raise ValueError(
            "{}: low limit greater than high limit [{}, {}]".format(
                obj.name,low_limit, high_limit
            )
        )
        return

    if not (low_limit <= value <= high_limit):
        raise ValueError(
            "{}: value {} outside of range: [{}, {}]".format(
                obj.name, value, low_limit, high_limit
            )
        )
    

def _abs_set_check_limits(obj, *args, group=None, wait=False, **kwargs):
    """
    Set a value. Optionally, wait for it to complete before continuing.

    Check the limits will not be exceeded before attempting to set

    Parameters
    ----------
    obj : Device
    group : string (or any hashable object), optional
        identifier used by 'wait'
    wait : boolean, optional
        If True, wait for completion before processing any more messages.
        False by default.
    args :
        passed to obj.set()
    kwargs :
        passed to obj.set()

    Yields
    ------
    msg : Msg

    See Also
    --------
    :func:`bluesky.plan_stubs.rel_set`
    :func:`bluesky.plan_stubs.wait`
    :func:`bluesky.plan_stubs.mv`
    """


    _check_limits(obj, args[0])

    if wait and group is None:
        group = str(uuid.uuid4())
    ret = yield Msg('set', obj, *args, group=group, **kwargs)
    if wait:
        yield Msg('wait', None, group=group)
    return ret



# Stepwise U17 DCM XAS
@parameter_annotation_decorator({
    "parameters": {
    "start_energy_eV": {
            "default": 7000,
            "min": 1000,
            "max": 400000,
            "step": 1,
        },
    "stop_energy_eV": {
            "default": 7100,
            "min": 1000,
            "max": 400000,
            "step": 1,
        },
    "step_eV": {
            "default": 1,
            "min": 0.001,
            "max": 10,
            "step": 0.01,
        },
    "int_time_s": {
            "default": 1,
            "min": 0.1,
            "max": 20,
            "step": 0.1,
        },
    }
})
def u17_dcm_xas(start_energy_eV:float=7000, stop_energy_eV:float=7100, step_eV:float=1,use_pfy_bruker:bool=True,v12_shutter:bool=0,int_time_s:float=1, md:dict=None):
    """
    move the u17_dcm from start energy to stop energy with a step size "step" in eV

    detectors kth00, kth01, kth25 and kth26 and the channeltron will always be read.
    
    The bruker is optionally read since it has a longer integration time

    Parameters
    ----------
    start_energy_eV: float
        The energy that the dcm will start at
    stop_energy_eV: float
        The energy that the dcm will stop scanning at
    step_eV: float
        The step size that the dcm will step while scanning.
    use_pfy_bruker: bool
        use the bruker mca detector      
    v12_shutter: bool
        open the v12 valve at the start of the scan, close it at the end
    int_time_s: float
        The number of seconds to integrate the detectors for at each step. Note that keithley detectors have a max integration time of 0.2s
    
    Notes
    ----

    """

    md = md or {}



    detectors = hard_detectors
    techniques = []
    if use_pfy_bruker:
        detectors.append(pfy_bruker.mca)
        techniques.append('NXxas_pfy')
    
    
    num = np.ceil((np.abs(stop_energy_eV-start_energy_eV))/step_eV)+1

    _md = {'detectors' : [det.name for det in detectors],
           'mono': u17_dcm.en.name,
           'plan_args': {   'start_energy': start_energy_eV,
                            'stop_energy': stop_energy_eV,
                            'energy_step': step_eV
                        },
           'num_points':num,
           'num': num,
           'plan_name': 'scan',
           'techniques': techniques,
           'hints': {}
           }

    _md.update(md or {})

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(2)

    yield from set_integration_time(int_time_s)
    
    if v12_shutter:
        yield from open_v12()

    yield from _scan(detectors, u17_dcm.en, start_energy_eV, stop_energy_eV, num, md=None)
    #yield from _scan(detectors, motor, start_energy_eV, stop_energy_eV, num, md=None) # for simulation with Regan

    if v12_shutter:
        yield from close_v12()


# 1D scan for endstation x, z or filters
@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "motor": {
            "annotation": "motor1",
            "enums": {"motor1": ["u17_dcm.en","ue48_pgm.en", "manipulator.x", "manipulator.y", "manipulator.z", "manipulator.r1", "manipulator.r2"]},
            "convert_device_names": True,
        },
        "start": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "stop": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "num": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },

    }
})
def scan(detectors,motor,start:float=0.0, stop:float=0.0, num:int=10,*, md:dict=None):

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(2)

    yield from _scan(detectors, motor, start, stop, num,md=md)  



@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "num": {
            "default": 10,
            "min": 0,
            "max": 1000000,
            "step": 1,
        },
    }
})
def count(detectors, num:int=10,delay:float=0.0, md:dict=None):

    md = md or {}
    _md = {
            'num_points':num,
           }

    _md.update(md or {})



    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(2)

    yield from _count(detectors, num,delay=delay,md=md)  

@parameter_annotation_decorator({
    "parameters": {
        "motor": {
            "annotation": "motor",
            "enums": {"motor": ["ue48_pgm.en", "u17_dcm.en","manipulator.x", "manipulator.y", "manipulator.z", "manipulator.r1", "manipulator.r2"]},
            "convert_device_names": True,
        },
        "position": {
            "default": 0.0,
            "min": -100000,
            "max": 100000,
            "step": 0.001,
        },
        "wait_for_completion": {
            "annotation": "wait",
            "enums": {"wait": ["True", "False"]},
        },
  
    }
})
def move(motor,wait_for_completion,position:float=0.0):

    if not (eval(wait_for_completion)):
        motor.move(position, wait=False)
    else:
        yield from _mov(motor, position) 


@parameter_annotation_decorator({
    "parameters": {
        "motor": {
            "annotation": "motor1",
            "enums": {"motor1": ["ue48_pgm.en", "u17_dcm.en", "manipulator.x", "manipulator.y", "manipulator.z", "manipulator.r1", "manipulator.r2"]},
            "convert_device_names": True,
        },
        "distance": {
            "default": 0.0,
            "min": -100000,
            "max": 100000,
            "step": 0.001,
        },
        
    }
})
def rel_move(motor,distance:float=0.0):
    yield from _movr(motor, distance)  

@parameter_annotation_decorator({
    "parameters": {
        "period": {
            "default": 1.0,
            "min": 0,
            "max": 100000,
            "step": 1.0,
        },
        
    }
})
def sleep(period:float=1.0):
    yield from _sleep(period)  

def open_soft_beamline():
    yield from _mov(ue48_v11, 1)

def close_soft_beamline():
    yield from _mov(ue48_v11, 0)

def open_hard_beamline():
    yield from _mov(u17_v11, 1)

def close_hard_beamline():
    yield from _mov(u17_v11, 0)

@parameter_annotation_decorator({
    "parameters": {
        "int_time": {
            "default": 0.1,
            "min": 0,
            "max": 10,
            "step": 0.1,
        },
        
    }
})
def set_integration_time( int_time:float=0.1):

    """
    Set the integration time of all detectors to int_time (s)
    """
    wait_group = "int_time_group"
    for detector in all_detectors:

        if hasattr(detector, "nplc"):
            nplc_time = (int_time*1000/20)
            if nplc_time >= 10:
                nplc_time = 10
            yield from _abs_set_check_limits(detector.nplc, nplc_time,timeout=5, group= wait_group)
            #yield from _configure(detector, {'nplc':nplc_time})
            print(f"Set {detector.name} to integration time {int_time} seconds.")

        elif isinstance(detector, ophyd.mca.EpicsMCA):

            #yield from _configure(detector, {'preset_real_time':int_time})ghd
            yield from _abs_set_check_limits(detector.preset_real_time, int_time, timeout=5,group= wait_group)
            print(f"Set {detector.name} to integration time {int_time} seconds")

    yield from _wait(wait_group)


# Flyscan UE48 PGM XAS
@parameter_annotation_decorator({
    "parameters": {
    "start_energy_eV": {
            "default": 400,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "stop_energy_eV": {
            "default": 450,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "energy_velocity_eV_s": {
            "default": 0.1,
            "min": 0.01,
            "max": 0.3,
            "step": 0.01,
        }
    }
})
def ue48_pgm_flyxas(use_bruker:bool=True,start_energy_eV:float=400, stop_energy_eV:float=450, energy_velocity_eV_s:float=0.1,v12_shutter:bool=0, md:dict=None):
    """
    move the ue48_pgm from start energy to stop energy at a constant energy velocity

    Detectors will be acquired as fast as possible given their individual integration times

    detectors beam_flux_diode or i0_mirror_current and the channeltron will always be read.
    
    The bruker is optionally read since it has a longer integration time

    Parameters
    ----------
    use_bruker : bool
        if enabled, also read from the bruker
    start_energy_eV: float
        the energy to start the monochromator at
    stop_energy_eV: float 
        the energy to stop the monochromator at
    energy_velocity_eV : float
        the rate to move the energy at
    v12_shutter : bool
        Open and close v12 around the measurement to protect the sample
    md : dict, optional
        metadata

    Notes
    ----

    """

    md = md or {}

    detectors = soft_detectors
    techniques = ['NXxas_tey']
    if use_bruker:
        detectors.append(pfy_bruker.mca)
        techniques.append('NXxas_pfy')

    _md = {'detectors' : [det.name for det in detectors],
           'mono': ue48_pgm.en.name,
           'plan_args':{
                'start_energy': start_energy_eV,
                'stop_energy': stop_energy_eV,
                'energy_velocity': energy_velocity_eV_s,
           },
           'plan_name': 'flyscan',
           'techniques': techniques,
           'hints': {}
           }

    _md.update(md or {})
    

    #configure keithleys for speed
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(1)
    
    shutter = None
    if v12_shutter:
        shutter = v12

    yield from _flyscan(detectors=detectors, flyer=ue48_pgm.en, start=start_energy_eV, stop=stop_energy_eV, vel=energy_velocity_eV_s, shutter=shutter, md=None)

# Stepwise UE48 PGM XAS
@parameter_annotation_decorator({
    "parameters": {   
    "start_energy_eV": {
            "default": 400,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "stop_energy_eV": {
            "default": 450,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "step_eV": {
            "default": 0.1,
            "min": 0.001,
            "max": 10,
            "step": 0.01,
        },
    }
})
def ue48_pgm_xas(use_bruker:bool=True,start_energy_eV:float=400, stop_energy_eV:float=450, step_eV:float=0.01,v12_shutter:bool=0, md:dict=None):
    """
    move the ue48_pgm from start energy to stop energy with a step size "step" in eV

    detectors tfy_photodiode, beam_flux_diode, i0_mirror_current and the channeltron will always be read.
    
    The bruker is optionally read since it has a longer integration time

    Parameters
    ----------
    use_bruker : bool
        if enabled, also read from the bruker
    start_energy: float
        the energy to start the monochromator at
    stop_energy: float 
        the energy to stop the monochromator at
    energy_velocity : float
        the rate to move the energy at
    md : dict, optional
        metadata

    Notes
    ----

    """

    md = md or {}

    detectors = soft_detectors
    techniques = ['NXxas_tey']
    if use_bruker:
        detectors.append(pfy_bruker.mca)
        techniques.append('NXxas_pfy')
    
    import numpy as np
    num = np.ceil((np.abs(stop_energy_eV-start_energy_eV))/step_eV)+1

    _md = {'detectors' : [det.name for det in detectors],
           'mono': ue48_pgm.en.name,
           'start_energy': start_energy_eV,
           'stop_energy': stop_energy_eV,
           'energy_step': step_eV,
           'num': num,
           'plan_name': 'scan',
           'techniques': techniques,
           'hints': {}
           }

    _md.update(md or {})

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(2)
    
    if v12_shutter:
        yield from open_v12()

    yield from _scan(detectors, ue48_pgm.en, start_energy_eV, stop_energy_eV, num, md=None)

    if v12_shutter:
        yield from close_v12()

@parameter_annotation_decorator({
    "parameters": {
        "horz_start": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "horz_stop": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "horz_num": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },
        "vert_start": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "vert_stop": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "vert_num": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },

    }
})
def sample_map(pfy_mca:bool=True,horz_start:float=0.0, horz_stop:float=0.0, horz_num:int=10,vert_start:float=0.0, vert_stop:float=0.0, vert_num:int=10,snake:bool=False, *, md:dict=None):

    detectors_list = hard_detectors + soft_detectors
    if pfy_mca:
        detectors_list.append(pfy_bruker.mca)

    #configure keithleys for reliable readings
    for detector in detectors_list:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.put(2)

    yield from _grid_scan(detectors_list, manipulator.z, vert_start, vert_stop,vert_num, manipulator.x, horz_start, horz_stop, horz_num, snake_axes=snake,md=md)  
