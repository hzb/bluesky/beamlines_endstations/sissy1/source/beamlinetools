from bessyii_devices.keithley import Keithley6517, Keithley6514
from ophyd.status import DeviceStatus, SubscriptionStatus
from ophyd import Device, Signal, Component as Cpt, EpicsSignal, EpicsSignalRO
from bessyii_devices.positioners import InternalSignal
import time

class OldKeithley6514(Keithley6514):

    int_time = None

class OldKeithley6517(OldKeithley6514):

       
    vsrc_ena            = Cpt(EpicsSignal, 'cmdVoltSrcEna', kind='config')
    vsrc                = Cpt(EpicsSignal, 'rbkVoltSrc' , write_pv='setVoltSrc',       kind='config')
    trig_mode    		= Cpt(EpicsSignal, 'rbkTrigCont', write_pv='setTrigCont',        string='True',      kind='config')    #single or continuous mode. Bypasses event detection (trig_src)

