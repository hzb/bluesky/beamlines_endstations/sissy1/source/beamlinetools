from ophyd import Device, Component as Cpt, PVPositioner
from ophyd import EpicsSignal, EpicsSignalRO


class SISSY1ManipulatorAxis(PVPositioner):

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name 
    
    setpoint        = Cpt(EpicsSignal,    'setAbsPos',kind = 'normal')
    readback        = Cpt(EpicsSignalRO,  'getPos',kind = 'hinted') 
    done            = Cpt(EpicsSignalRO,  'ActualBusy', kind = 'omitted' )
    velocity        = Cpt(EpicsSignal, 'getTargetVel' , write_pv= 'setTargetVel',kind = 'config')

class SISSY1Manipulator(Device):

    x = Cpt(SISSY1ManipulatorAxis, 'X')
    y = Cpt(SISSY1ManipulatorAxis,'Y')
    z = Cpt(SISSY1ManipulatorAxis,'Z')
    r1  = Cpt(SISSY1ManipulatorAxis,'R1')
    r2  = Cpt(SISSY1ManipulatorAxis,'R2')

    transfer_on        = Cpt(EpicsSignal, 'getTransferOn',  write_pv= 'setTransferOn',kind = 'config')
    heating_on        = Cpt(EpicsSignal, 'getHeatingOn', write_pv= 'setHeatingOn',kind = 'config')
    cooling_on         = Cpt(EpicsSignal, 'getCoolingOn',  write_pv= 'setCoolingOn',kind = 'config')
    
