from ophyd import Device, Component as Cpt, EpicsSignalRO
from ophyd import DeviceStatus, Signal


class AnalyserCam(Device):

    """
    A device that collects together the parameters of the XPS Analyser Camera

    This allows us to scan the position of the manipulator and maximise the counts to optimise the sample position

    analyser_cam = AnalyserCam('SISSY1EX:PEAK:CAM:', name='analyser_cam)
    """

    counts_sec = Cpt(EpicsSignalRO, 'SPOTCNTRB', kind='hinted')
    total_intensity_sec = Cpt(EpicsSignalRO, 'TOTINTRB', kind='hinted')
    exposure_time_sec = Cpt(EpicsSignalRO, 'EXPOSURERB', kind='config')
    _trigger_counter = Cpt(Signal,value=0, kind='omitted')

    def trigger(self):
           
        # Create a callback which will ensure we always wait for a fresh value. In the worst case we might have to wait 2 seconds.
        def new_value(*,old_value,value,**kwargs):          

            # If we have done all the required tiggers
            if self._trigger_counter.get() == 1:

                #Reset the trigger counter for next acq
                self._trigger_counter.put(0)

                # Clear the subscription.
                self.counts_sec.clear_sub(new_value)

                # Set Finished
                status.set_finished()

            # If we still have triggers to issue
            else:
                
                # Increment the trigger counter
                self._trigger_counter.put( self._trigger_counter.get() + 1)
            
        #Clear the trigger counter
        self._trigger_counter.put(0)

        #Create the status object
        status = DeviceStatus(self.counts_sec,timeout = 10.0)

        #Connect the callback that will set finished and clear sub
        self.counts_sec.subscribe(new_value,event_type=Signal.SUB_VALUE,run=False)
        
        
        return status
